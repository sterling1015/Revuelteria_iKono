$(document).ready(iniciar)

function iniciar() {
	// Funcion para obtener el JSON e iterarlo 
	var productos = 'https://demo4010019.mockable.io/products'
	$.getJSON(productos,function(response){
		$("#productos").html("");
		$.each(response,function obt(key,value) {
			var html = "";
			html+="<div class='col-md-3 panel panel-default'>"
			html+="<div class='panel-body'>"
					html+="<h2>"+value['name']+"</h2>";
					html+="<img class='' src='"+value['url']+"' />"
					html+="	 <h3><b>$"+value['price']+"</b></h3>";
					html+="  <center><button type='button' class='more btn btn-primary' data-toggle='modal' data-target='#vermas' name='"+value['name']+"' price='"+value['price']+"' unit='"+value['unit']+"' url='"+value['url']+"'>Ver más </button></center>"
			html+="</div>"
			html+='</div>'
			$("#productos").append(html)
			$(".more").on("click",infoproducto)
		});
	});
}
function infoproducto() {
	// Funcion que imprime el modal donde muestra la informacion adicional
	var name= $(this).attr('name')
	var price= $(this).attr('price')
	var unit= $(this).attr('unit')
	var url= $(this).attr('url')
	var modal=""
	modal+='<div class="modal fade" id="vermas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">';
	modal+='  <div class="modal-dialog" role="document">';
	modal+='    <div class="modal-content">';
	modal+='      <div class="modal-header">';
	modal+='        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
	modal+='        <h3 class="modal-title" id="myModalLabel">Detalles <i class="glyphicon glyphicon-list-alt"></i> </h3>';
	modal+='      </div>';
	modal+='      <div class="modal-body row">';
	modal+='        <div class="col-md-4"> <b>Nombre: </b>'+name+'</div>';
	modal+='        <div class="col-md-4"> <b>Precio: </b> $'+price+'</div>';
	modal+='        <div class="col-md-4"> <b>Unidad de medida: </b>'+unit+'</div>';
	modal+='		<img src="'+url+'" />';
	modal+='      </div>';
	modal+='      <div class="modal-footer">';
	modal+='        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>';
	modal+='      </div>';
	modal+='    </div>';
	modal+='  </div>';
	modal+='</div>';
	$("#modalvermas").html(modal)
	
}
